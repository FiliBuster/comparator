import java.util.Comparator;

/**
 * Created by ubuntu on 15.07.16.
 */
public class VersionComparator implements Comparator<String> {


    public int compare(String s, String t1) {
        String[] firstArray = s.split("\\.");
        String[] secondaArray = t1.split("\\.");

        Double firstArrayValue = giveValueOFArray(firstArray);
        Double secondArrayValue = giveValueOFArray(secondaArray);

        return firstArrayValue.compareTo(secondArrayValue);
    }

  private Double giveValueOFArray(String[] array){
      Double arrayValue = new Double("0");
      Double tempParser;

      for(int i = 0; i<= (array.length -1); i++){
          if(Character.isLetter(array[i].charAt(0))){
              tempParser = ((double) Character.getNumericValue(array[i].charAt(0)) / (Math.pow(10, (double) i)));
          }else{

              tempParser = Double.parseDouble((array[i])) / (Math.pow(10, (double)i));}
          arrayValue += tempParser;

      }

      return arrayValue;
  }

}
